---
layout: page
title: Resume
permalink: /resume/
---
## Work History

### Australian Bureau of Meteorology

Application Dev & Support<br>
August 2012 -- present

- WMO Information System (WIS) (5/8/12 to present)
    - **Integration Development:** Designed and implemented the dissemination harnesses, which is a J2EE application responsible for sending out Email and FTP specified by users of the WIS portal software (OpenWIS) running at the Bureau.  This is currently running in production in a Jboss 6 environment and integrates with WIS via a SOAP endpoint.  Currently developing a second integration point which will be use to extract data specifically provided by the Bureau and make it available to users of WIS.
    - **Application Development:** Participating in development activities with partners of the OpenWIS Association on OpenWIS itself.  Designed and implemented additional features, and was involved in testing and bug-fixing the version currently in development.  Currently a member of the OpenWIS technical management committee, which includes representatives from the met services of the UK, France, United States and South Korea.
    - **Application Support and User Training:** Performing developer level operational support for OpenWIS portal website (http://wis.bom.gov.au/). This includes version upgrades, data maintenance tasks, application and environment configuration (i.e. PostgreSQL database) and have answered developer level support queries.  Have also been involved in end user training on how to use the software by presenting in workshops hosted within the Bureau, and also hosted by end users of the system.
- Message Switching System Web Viewer (9/13 to present)
    - **Application Development:** Designed and implemented a web-based viewer for the message switching system currently used within the Bureau.  This is currently running in production and is implemented as a Java web application running in Tomcat 7 and includes technologies like Backbone.js, jQuery and a JNI layer wrapping the C libraries of the message switching system.

### Object Consulting Pty. Ltd.

Product Specialist<br>
February 2008 -- August 2012

- Products and Assets (4/2/08 to 3/8/12)
    - **Application Development:** Actively participated in development of medical document management system.  This includes design and implementation of new functionality such as XForm based electronic form subsystem, personalisation and patient label printing module.  Additionally, maintained and enhanced the existing code base.  Have also been involved in building tools (e.g. a randomised test data generator and a data maintenance tool) to assist development and prototyping work for sales demos.
    - **System Administration:** Performed basic system administration to internal, customer UAT and customer production environments.  This included version upgrades, data migrations, data maintenance tasks and application configuration.
    - **Consulting:** Participated in rollout, training, load testing, disaster recovery rehearsals and customer support of medical document management system.
- Telstra (3/1/10 to 30/6/10)
    - **Application Development:** Actively participated in development of internal pricing tool.  This included design and implementation of new functionality as well as maintenance and defect fixing of existing code base.
    - **System Administration:** Performed upgrades of pricing tool to UAT environment and production environment.

### Synergetics Pty. Ltd.

Programmer<br>
December 2006 -- January 2008

- **Code Design:** Designed and implemented a program to find the optimal configuration for settings to a model to find minimum violations to a set of constraints whilst also attempting to find the minimum cost of the configuration.
- **Code Management:** Maintained a number of models to assist in the creation of numerical data.
- **Data Management:** Designed a number of utility programs (including some scripts) to assist in analysing and rearranging data.

### Object Consulting Pty. Ltd.

Summer Vacation Work<br>
January 2006 -- February 2006

- **Test Analysis:** Performed manual execution of test cases as well as the development of tools to assist in execution of automated tests.
- **Business Analysis:** Developed mock-up UI screens for requirement verification as well as provided assistance for updating the Software Requirement Specifications.

## Skills

### Technical

- Computer Languages:
    - Java SE 1.7 (expert level)
    - MySQL (expert level)
    - HTML, CSS, JavaScript (expert level)
    - XML, XSLT, XForms (expert level)
    - SQL, JPAQL (expert level)
    - Perl, Python, Ruby, PHP, Lua, Shell Script (intermediate level)
    - Go (intermediate level)
    - C++, C (intermediate level)
    - C# (basic level)
    - Delphi (intermediate level)
    - Visual Basic (basic level)
- Build Technologies
    - Ant
    - Maven
    - Gradle (basic level)
    - GNU Make
- Application Containers:
    - JBoss Application Server 7 (J2EE 6)
    - Tomcat 6/7
- Database Products
    - MySQL 5
    - Oracle
    - PostgreSQL 8/9
- Frameworks
    - Spring
    - Hibernate, JPA
    - JUnit
    - JMockIT
    - Log4J
    - Mobile Framework
    - Android
- Presentation Framework
    - JSP
    - Struts
    - Swing
- Security Products
    - LDAP (development)
- Tools
    - Eclipse
    - JMeter
    - Mercury Test Director
    - NetBeans
    - Microsoft Office: Word, Excel, PowerPoint, Visio
    - JIRA
    - Confluence
    - Microsoft Visual Studios
    - Git
    - Mercurial
- Web Servers
    - Apache Web Server
- Operating Systems
    - Microsoft Windows (usage, utilities, development)
    - OpenSUSE Linux 11+ (usage, utilities, development)
    - Ubuntu Linux 9.4+ (usage, utilities, development)
    - Red-Hat Enterprise Linux 5 (usage)
- Documentation
    - UML
    - Confluence

### Personal

- Consulting Skills
- Analytical Problem Solving
- Quick Learning

### Education

- Secondary
    - Marcellin College, Bulleen
    - VCE ENTER Score: 97.1
- Tertiary
    - University of Melbourne, Parkville
    - Bachelor of Computer Science, Honours (1st Class)

### Interests

- Music
    - Piano, Viola, Theory
    - Composition
- Computers
    - Development of Games and Utility Programs
    - General Software Development
    - Arduino Microprocessor Programming
- Others
    - Restricted Pilot Licence